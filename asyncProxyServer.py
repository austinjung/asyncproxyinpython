from aiohttp import web, ClientSession
import async_timeout
import time
from datetime import timedelta


class AsyncProxyHandler(object):

    def __init__(self, target_url='https://www.youtube.com/'):
        self.request_headers = {}
        self.response_headers = {}
        self.target_url = target_url
        self.startup_time = time.time()
        self.bytes_transferred = 0

    async def post_async(self, session, url, data):
        with async_timeout.timeout(30):
            async with session.post(url, data=data) as response:
                if url not in self.response_headers:
                    self.response_headers[url] = response.headers
                return await response.text()

    async def handle_post(self, request):
        target_url = self.target_url + request.match_info.get('tail', '')
        self.build_request_headers(request, target_url)
        async with ClientSession(headers=self.request_headers[target_url]) as session:
            data = await request.post()
            response_text = await self.post_async(session, target_url, data)
            self.update_usage(len(response_text))
            return web.Response(text=response_text, headers=self.build_response_headers(target_url))

    async def fetch_async(self, session, url):
        with async_timeout.timeout(30):
            async with session.get(url) as response:
                if url not in self.response_headers:
                    self.response_headers[url] = response.headers
                return await response.text()

    async def handle_get(self, request):
        if 'target_url' in request.query:
            self.target_url = request.query.get('target_url', self.target_url)
            if self.target_url[-1] != '/':
                self.target_url = self.target_url + '/'
        if request.path == '/stats' or request.path == '/stats/':
            return self.handle_statistics(request)
        target_url = self.target_url + request.match_info.get('tail', '')
        if self.range_request_unsatisfiable(request):
            return self.handle_unsatisfiable_range_header(request)
        self.build_request_headers(request, target_url)
        async with ClientSession(headers=self.request_headers[target_url]) as session:
            response_text = await self.fetch_async(session, target_url)
            self.update_usage(len(response_text))
            return web.Response(text=response_text, headers=self.build_response_headers(target_url))

    def handle_unsatisfiable_range_header(self, request):
        error_message = """
            <!DOCTYPE html>
                <head>
                    <title>AsyncProxy - 416: Requested Range not satisfiable</title>
                </head>
                <body>
                    <h4>AsyncProxy - 416: Requested Range not satisfiable</h4>
                    <p>The values of header range parameter is invalid.<br />
                    <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests">
                    https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests
                    </a></p>
                </body>
            </html>"""
        return web.Response(text=error_message, headers={'Content-Type': 'text/html'})

    def handle_statistics(self, request):
        response_text = """<!DOCTYPE html>
                        <html lang="en"><head><meta name="viewport" content="width=device-width, initial-scale=1">
                        <!-- Latest compiled and minified CSS -->
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
                        integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
                        crossorigin="anonymous"></head>
                            <body>
                                <div class="container">
                                    <div class="page-header">
                                        <h1>AsyncProxy <small>statistics</small></h1>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">Uptime</div>
                                                <div class="panel-body"><center><h2>{uptime}</center></h2></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">Bytes transferred</div>
                                                <div class="panel-body"><center><h2>{bytes_transferred}</h2></center></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="panel panel-info">
                                                <div class="panel-heading">KBytes transferred</div>
                                                <div class="panel-body"><center><h2>{kbytes_transferred}</center></h2></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="panel panel-warning">
                                                <div class="panel-heading">MBytes transferred</div>
                                                <div class="panel-body"><center><h2>{mbytes_transferred}</center></h2></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </body>
                        </html>""".format(uptime=self.get_uptime(),
                                          bytes_transferred=self.get_bytes_transferred('Bytes'),
                                          kbytes_transferred=self.get_bytes_transferred('KBytes'),
                                          mbytes_transferred=self.get_bytes_transferred('MBytes'))
        return web.Response(text=response_text, headers={'Content-Type': 'text/html'})

    def update_usage(self, bytes_transferred):
        self.bytes_transferred += bytes_transferred

    def get_bytes_transferred(self, output_format):
        if output_format == 'Bytes':
            return '%d %s' % (int(self.bytes_transferred), output_format)
        elif output_format == 'KBytes':
            return '%d %s' % (int(self.bytes_transferred / 1024), output_format)
        elif output_format == 'MBytes':
            return '%d %s' % (int(self.bytes_transferred / 1048576), output_format)
        else:
            return ''

    def get_uptime(self):
        delta = timedelta(seconds=time.time() - self.startup_time)
        return ('%d days' % delta.days) + ', ' + time.strftime('%H:%M:%S', time.gmtime(delta.seconds))

    def range_request_unsatisfiable(self, request):
        if 'Range' in request.headers:
            range_query = request.headers['Range'].strip()
            if not range_query.startswith('bytes='):
                return True
            ranges = range_query.replace('bytes=', '').split(',')
            for range in ranges:
                range_values = range.strip().split('-')
                if len(range_values) < 1 or len(range_values) > 2:
                    return True
                try:
                    int(range_values[0])
                    if len(range_values) == 2 and range_values[1] != '':
                        int(range_values[1])
                except ValueError:
                    return True
        return False

    def build_request_headers(self, request, target_url):
        request_headers = {}
        for k, v in request.headers.items():
            if k in ['Range']:
                request_headers[k] = v
            elif k in ['Referer']:
                request_headers[k] = self.target_url
        self.request_headers[target_url] = request_headers

    def build_response_headers(self, target_url):
        response_headers = {}
        for k, v in self.response_headers[target_url].items():
            if k in ['Content-Type', 'Cache-Control']:
                response_headers[k] = v
        return response_headers


app = web.Application()
async_proxy_handler = AsyncProxyHandler()
app.router.add_route('GET', '/{tail:.*}', async_proxy_handler.handle_get)
app.router.add_route('POST', '/{tail:.*}', async_proxy_handler.handle_post)

web.run_app(app, port=8080)
