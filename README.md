# AsyncProxy - Python Programming Task

Your task is to build an asynchronous HTTP proxy (see definition in
[RFC2616](https://www.ietf.org/rfc/rfc2616.txt)) complying to the requirements
specified below.

## Requirements

1. Range requests must be supported as defined in
[RFC2616](https://www.ietf.org/rfc/rfc2616.txt), but also via `range` query
parameter.

2. HTTP 416 error must be returned in case where both header and query parameter are
specified, but with a different value.

3. Program must start with a single command `docker-compose up`.

4. Proxy must be reachable at `http://<docker-host>:8080` .

5. Usage statistics must be available at `http://<docker-host>:8080/stats`

  * total bytes transferred
  * uptime

6. Code must run with Python 3.5+.

7. Code must be delivered as a link to public GitHub repository.

## Design

1. I built a simple AsyncProxy server based on [aiohttp](http://aiohttp.readthedocs.io/en/stable/). The reasons are:
    - aiohttp is based on [asyncio — Asynchronous I/O, event loop, coroutines and tasks module og python 3.4+](https://docs.python.org/3.6/library/asyncio.html#module-asyncio)
    - aiohttp supports both Client and HTTP Server. 
    - aiohttp supports both Server WebSockets and Client WebSockets out-of-the-box.
    - aiohttp Web-server has Middlewares, Signals and pluggable routing.
    
2. The default proxy target server is set to http://youtube.com
    - Browsing **http://localhost:8080** will response with the content of **http://youtube.com**
    - Browsing **http://localhost:8080/stats** will give **the current stats of the AsyncProxy server**.
    - Browsing **http://localhost:8080/?target_url=http://python.org** will **change proxy target to python.org** and response with the content of http://python.org 


## Deployment

1. This project repository is [https://bitbucket.org/austinjung/asyncproxyinpython](https://bitbucket.org/austinjung/asyncproxyinpython)
2. The project repository is linked with [Austin's Docker Cloud](https://cloud.docker.com/swarm/austinjung/repository/registry-1.docker.io/austinjung/async-http-proxy-python/general)
3. In your docker, run the following line.
    ```
    $ docker run -p 8080:8080 austinjung/async-http-proxy-python:latest
    ```
    or 
    ```
    clone the project repository
    cd /to/the_top_folder_of_the_project
    $ docker-compose up
    ```

## Things do-to

1. Some images or contents are blocked by 'Referrer Policy: no-referrer-when-downgrade', 'Access-Control-Allow_Origin', or 'cross-origin frame' etc.
2. Do proper post method.
and more